<?php

namespace IncGroup\ParticipantesBundle\Controller;

use IncGroup\ParticipantesBundle\Entity\Participantes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ParticipantesController extends Controller
{
    public function indexAction()
    {
        //return $this->render('IncGroupParticipantesBundle:Default:index.html.twig');
        return new Response('Participantes Modulo');
    }

    public function crearUsuariosAction(){
      //return new Response('Subir Participantes');
      $form = $this->createFormBuilder()
        ->add('submitFile', 'file', array('label' => 'csv para subir'))
        ->getForm();

      return $this->render('IncGroupParticipantesBundle:participantes:index.html.twig',
                            array('form' => $form->createView())
                          );
    }

    public function guardarUsuariosAction(Request $request){
      //print_r($request->files);
      $form = $this->createFormBuilder()
        ->add('submitFile', 'file', array('label' => 'csv para subir'))
        ->getForm();


        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('IncGroupParticipantesBundle:Participantes');
        $valid_upload = 1;
        // Check if we are posting stuff
        if ($request->getMethod('post') == 'POST') {
            // Bind request to the form
            $form->bind($request);
            // If form is valid
            if ($form->isValid()) {
                 $file = $form->get('submitFile');
                 $file_csv = $file->getData()->getPathName();
                 $file_extension = $file->getData()->getClientOriginalExtension();
                 $valid_upload = 1;

                 if($file_extension == 'csv')
                 {
                   $row = 1;
                   if (($handle = fopen($file_csv, "r")) !== FALSE) {
                     while (($data = fgetcsv($handle, 100, ";")) !== FALSE) {
                       $num = count($data);
                       //echo "<p> $num fields in line $row: <br /></p>\n";

                       if($row > 1){
                         for ($c=0; $c < $num; $c++) {
                           $datos_trama = explode(',',$data[$c]);
                           //var_dump($datos_trama);
                           $participante = $repository->findOneByIdExcel((int)$datos_trama[0]);

                           if (!$participante) {
                             $participante = new Participantes();
                             $participante->setIdExcel((int)$datos_trama[0]);
                             $participante->setNombre($datos_trama[1]);
                             $participante->setCedula($datos_trama[2]);
                             $participante->setEdad((int)$datos_trama[3]);
                             $participante->setGenero((int)$datos_trama[4]);

                           }else {
                             $participante->setNombre($datos_trama[1]);
                             $participante->setCedula($datos_trama[2]);
                             $participante->setEdad((int)$datos_trama[3]);
                             $participante->setGenero((int)$datos_trama[4]);
                           }

                           $em->persist($participante);
                           $em->flush();
                           $em->clear();
                         }

                       }

                       $row++;
                     }
                     fclose($handle);
                   }

                 }else {
                   $request->getSession()
                       ->getFlashBag()
                       ->add('error', 'El Archivo no es de extension csv');

                   $valid_upload = 0;
                 }//end extension validation

            }

         }

         if ($valid_upload == 1) {
           $participantes = $repository->findAll();
           return $this->render('IncGroupParticipantesBundle:participantes:list.html.twig',
                                 array('participantes' => $participantes)
                               );
         }else{
           return $this->render('IncGroupParticipantesBundle:participantes:index.html.twig',
                                 array('form' => $form->createView())
                               );
         }
    }
}
