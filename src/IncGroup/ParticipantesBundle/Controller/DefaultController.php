<?php

namespace IncGroup\ParticipantesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IncGroupParticipantesBundle:Default:index.html.twig');
    }
}
