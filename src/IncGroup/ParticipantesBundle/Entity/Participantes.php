<?php

namespace IncGroup\ParticipantesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participantes
 *
 * @ORM\Table(name="participantes")
 * @ORM\Entity(repositoryClass="IncGroup\ParticipantesBundle\Repository\ParticipantesRepository")
 */
class Participantes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_excel", type="integer", unique=true)
     */
    private $idExcel;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="cedula", type="string", length=90)
     */
    private $cedula;

    /**
     * @var int
     *
     * @ORM\Column(name="edad", type="integer")
     */
    private $edad;

    /**
     * @var int
     *
     * @ORM\Column(name="genero", type="smallint")
     */
    private $genero;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idExcel
     *
     * @param integer $idExcel
     *
     * @return Participantes
     */
    public function setIdExcel($idExcel)
    {
        $this->idExcel = $idExcel;

        return $this;
    }

    /**
     * Get idExcel
     *
     * @return int
     */
    public function getIdExcel()
    {
        return $this->idExcel;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Participantes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     *
     * @return Participantes
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return string
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set edad
     *
     * @param integer $edad
     *
     * @return Participantes
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return int
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set genero
     *
     * @param integer $genero
     *
     * @return Participantes
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return int
     */
    public function getGenero()
    {
        return $this->genero;
    }
}
