<?php

namespace IncGroup\VariablesBundle\Controller;

use IncGroup\VariablesBundle\Entity\Variable;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class VariablesController extends Controller
{
    public function indexAction()
    {
        return $this->render('IncGroupVariablesBundle:Default:index.html.twig');
    }

    public function crearVariablesAction(){
      //return new Response('Subir Participantes');
      $form = $this->createFormBuilder()
        ->add('submitFile', 'file', array('label' => 'csv para subir'))
        ->getForm();

      return $this->render('IncGroupVariablesBundle:variables:index.html.twig',
                            array('form' => $form->createView())
                          );
    }

    private function calcPremio($datos_trama, $cumplimiento){
      $premio = 'NO CLASIFICA';

      if((int)$datos_trama[1] == 1){
        //Ventas
        if($cumplimiento >= 0.95 && $cumplimiento < 1 ){
          $premio = 'bronce';
        }else if($cumplimiento >= 1 && $cumplimiento < 1.05 ){
          $premio = 'plata';
        }else if($cumplimiento >= 1.05 ){
          $premio = 'oro';
        }
      }else if((int)$datos_trama[1] == 2) {
        //Efectividad
        if($cumplimiento >= 0.85 && $cumplimiento < 0.91 ){
          $premio = 'c';
        }else if($cumplimiento >= 0.91 && $cumplimiento < 0.96 ){
          $premio = 'b';
        }else if($cumplimiento >= 0.96 ){
          $premio = 'a';
        }
      }

      return $premio;
    }

    public function guardarVariablesAction(Request $request){
      $form = $this->createFormBuilder()
        ->add('submitFile', 'file', array('label' => 'csv para subir'))
        ->getForm();

        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository('IncGroupVariablesBundle:Variable');
        $valid_upload = 1;

        // Check if we are posting stuff
        if ($request->getMethod('post') == 'POST') {
          $form->bind($request);
          if ($form->isValid()) {
            $file = $form->get('submitFile');
            $file_csv = $file->getData()->getPathName();
            $file_extension = $file->getData()->getClientOriginalExtension();

            if($file_extension == 'csv')
            {
              $row = 1;
              if (($handle = fopen($file_csv, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 100, ";")) !== FALSE) {
                  $num = count($data);
                  //echo "<p> $num fields in line $row: <br /></p>\n";
                  if($row > 1){
                    for ($c=0; $c < $num; $c++) {
                      $datos_trama = explode(',',$data[$c]);

                      $user = $this->getDoctrine()->getRepository('IncGroupParticipantesBundle:Participantes')->findOneByIdExcel((int)$datos_trama[0]);

                      $variable = $repository->findOneBy(
                        array('variable' => (int)$datos_trama[1],
                        'mes' => (int)$datos_trama[2],
                        'participante' => $user)
                      );

                      if($user){
                        $cumplimiento = (float)$datos_trama[3]/(float)$datos_trama[4];

                        $premio = $this->calcPremio($datos_trama, $cumplimiento);

                        if(!$variable){
                          $variable = new Variable();
                          $variable->setParticipante($user);
                          $variable->setVariable((int)$datos_trama[1]);
                          $variable->setMes((int)$datos_trama[2]);
                          $variable->setResultado((int)$datos_trama[3]);
                          $variable->setObjetivo((int)$datos_trama[4]);
                          $variable->setCumplimiento($cumplimiento);
                          $variable->setPremio($premio);
                        }else{
                          $variable->setResultado((int)$datos_trama[3]);
                          $variable->setObjetivo((int)$datos_trama[4]);
                          $variable->setCumplimiento($cumplimiento);
                          $variable->setPremio($premio);
                        }
                        //print_r($variable);
                        $em->persist($variable);
                        $em->flush();
                        $em->clear();
                      }


                    }
                  }
                  $row++;
                }
                fclose($handle);
              }

            }else{
              $request->getSession()
                  ->getFlashBag()
                  ->add('error', 'El Archivo no es de extension csv');

                  $valid_upload = 0;
            }//end extension validation
          }
        }

        if ($valid_upload == 1) {
          $variables = $repository->findAll();
          return $this->render('IncGroupVariablesBundle:variables:list.html.twig',
            array('variables' => $variables)
          );
        }else{
          return $this->render('IncGroupVariablesBundle:variables:index.html.twig',
                                array('form' => $form->createView())
                              );
        }
    }

    public function liquidarVariablesAction(){
      $em = $this->getDoctrine()->getManager();
      $repository = $this->getDoctrine()->getRepository('IncGroupVariablesBundle:Variable');
      $variables = $repository->findAll();
      $liquidacion = [];

      foreach ($variables as $key => $value) {
        $index_mount = $value->getMes().$value->getParticipante()->getIdExcel();

        if(!array_key_exists($value->getMes().$value->getParticipante()->getIdExcel(), $liquidacion)){


          $liquidacion[$index_mount] = (object) array('id_user_excel'            => $value->getParticipante()->getIdExcel(),
                                                      'name_user'                => $value->getParticipante()->getNombre(),
                                                      'mes'                      => $value->getMes(),
                                                      'cumplimiento_ventas'      => '',
                                                      'cumplimiento_efectividad' => '',
                                                      'categoria'                => '',
                                                      'tipo'                     => '',
                                                      'dinero'                   => 0
                                                    );

          if($value->getVariable() == 1){
            $liquidacion[$index_mount]->cumplimiento_ventas = $value->getCumplimiento();
            $liquidacion[$index_mount]->categoria = $value->getPremio();
          }

          if($value->getVariable() == 2){
            $liquidacion[$index_mount]->cumplimiento_efectividad = $value->getCumplimiento();
            $liquidacion[$index_mount]->tipo = $value->getPremio();
          }

        }else{
          if($value->getVariable() == 1){
            $liquidacion[$index_mount]->cumplimiento_ventas = $value->getCumplimiento();
            $liquidacion[$index_mount]->categoria = $value->getPremio();
          }

          if($value->getVariable() == 2){
            $liquidacion[$index_mount]->cumplimiento_efectividad = $value->getCumplimiento();
            $liquidacion[$index_mount]->tipo = $value->getPremio();
          }

          $liquidacion[$index_mount]->dinero = $this->calcMoney($liquidacion[$index_mount]->categoria, $liquidacion[$index_mount]->tipo);

        }
      }

      $response = $this->createExcelLiquidacion($liquidacion);

      return $response;
    }

    private function createExcelLiquidacion($liquidacion){
      // ask the service for a Excel5
       $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

       $phpExcelObject->getProperties()->setCreator("liuggio")
           ->setLastModifiedBy("Gabriel Duncan")
           ->setTitle("Liquidación")
           ->setSubject("Office 2005 XLSX Test Document")
           ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
           ->setKeywords("office 2005 openxml php")
           ->setCategory("Test result file");

       //titulos
       $phpExcelObject->setActiveSheetIndex(0)
          ->setCellValue('A1', 'id')
          ->setCellValue('B1', 'id_user')
          ->setCellValue('C1', 'Nombre')
          ->setCellValue('D1', 'Mes')
          ->setCellValue('E1', '%Cumplimiento Ventas')
          ->setCellValue('F1', '%Cumplimiento Efectividad')
          ->setCellValue('G1', 'Categoria')
          ->setCellValue('H1', 'Tipo')
          ->setCellValue('I1', 'Dinero');

       $row = 2;
       foreach ($liquidacion as $key => $value) {
         //var_dump($value);

         $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A'.$row, $row)
            ->setCellValue('B'.$row, $value->id_user_excel)
            ->setCellValue('C'.$row, $value->name_user)
            ->setCellValue('D'.$row, $value->mes)
            ->setCellValue('E'.$row, (round($value->cumplimiento_ventas*100)).'%')
            ->setCellValue('F'.$row, (round($value->cumplimiento_efectividad*100)).'%')
            ->setCellValue('G'.$row, $value->categoria)
            ->setCellValue('H'.$row, $value->tipo)
            ->setCellValue('I'.$row, $value->dinero);

          $row++;
       }


       $phpExcelObject->getActiveSheet()->setTitle('Simple');
       // Set active sheet index to the first sheet, so Excel opens this as the first sheet
       $phpExcelObject->setActiveSheetIndex(0);

       // create the writer
       $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
       // create the response
       $response = $this->get('phpexcel')->createStreamedResponse($writer);
       // adding headers
       $dispositionHeader = $response->headers->makeDisposition(
           ResponseHeaderBag::DISPOSITION_ATTACHMENT,
           'stream-file.xls'
       );
       $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
       $response->headers->set('Pragma', 'public');
       $response->headers->set('Cache-Control', 'maxage=1');
       $response->headers->set('Content-Disposition', $dispositionHeader);

       return $response;
    }

    private function calcMoney($cat_premio, $tipo_premio){
      $money = 0;

      if(strtolower($cat_premio) == 'bronce' && strtolower($tipo_premio) == 'c'){
        $money = 62.50;
      }
      if(strtolower($cat_premio) == 'bronce' && strtolower($tipo_premio) == 'b'){
        $money = 93.75;
      }
      if(strtolower($cat_premio) == 'bronce' && strtolower($tipo_premio) == 'a'){
        $money = 125;
      }

      if(strtolower($cat_premio) == 'plata' && strtolower($tipo_premio) == 'c'){
        $money = 87.50;
      }
      if(strtolower($cat_premio) == 'plata' && strtolower($tipo_premio) == 'b'){
        $money = 131.25;
      }
      if(strtolower($cat_premio) == 'plata' && strtolower($tipo_premio) == 'a'){
        $money = 175;
      }

      if(strtolower($cat_premio) == 'oro' && strtolower($tipo_premio) == 'c'){
        $money = 125;
      }
      if(strtolower($cat_premio) == 'oro' && strtolower($tipo_premio) == 'b'){
        $money = 187.5;
      }
      if(strtolower($cat_premio) == 'oro' && strtolower($tipo_premio) == 'a'){
        $money = 250;
      }

      return $money;
    }
}
