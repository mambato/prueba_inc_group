<?php

namespace IncGroup\VariablesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IncGroupVariablesBundle:Default:index.html.twig');
    }
}
