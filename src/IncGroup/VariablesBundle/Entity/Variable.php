<?php

namespace IncGroup\VariablesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variable
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity(repositoryClass="IncGroup\VariablesBundle\Repository\VariableRepository")
 */
class Variable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="variable", type="integer")
     */
    private $variable;

    /**
	   * @ORM\ManyToOne(targetEntity="IncGroup\ParticipantesBundle\Entity\Participantes", inversedBy="participante_id")
	   * @ORM\JoinColumn(name="participante_id", referencedColumnName="id")
	   */
	  protected $participante;

    /**
     * @var int
     *
     * @ORM\Column(name="mes", type="integer")
     */
    private $mes;

    /**
     * @var int
     *
     * @ORM\Column(name="resultado", type="integer")
     */
    private $resultado;

    /**
     * @var int
     *
     * @ORM\Column(name="objetivo", type="integer")
     */
    private $objetivo;

    /**
     * @var float
     *
     * @ORM\Column(name="cumplimiento", type="float")
     */
    private $cumplimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="premio", type="string", length=15)
     */
    private $premio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variable
     *
     * @param integer $variable
     *
     * @return Variable
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Get variable
     *
     * @return int
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     *
     * @return Variable
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return int
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set resultado
     *
     * @param integer $resultado
     *
     * @return Variable
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Get resultado
     *
     * @return int
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Set objetivo
     *
     * @param integer $objetivo
     *
     * @return Variable
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;

        return $this;
    }

    /**
     * Get objetivo
     *
     * @return int
     */
    public function getObjetivo()
    {
        return $this->objetivo;
    }

    /**
     * Set cumplimiento
     *
     * @param float $cumplimiento
     *
     * @return Variable
     */
    public function setCumplimiento($cumplimiento)
    {
        $this->cumplimiento = $cumplimiento;

        return $this;
    }

    /**
     * Get cumplimiento
     *
     * @return float
     */
    public function getCumplimiento()
    {
        return $this->cumplimiento;
    }

    /**
     * Set participante
     *
     * @param \IncGroup\ParticipantesBundle\Entity\Participantes $participante
     *
     * @return Variable
     */
    public function setParticipante(\IncGroup\ParticipantesBundle\Entity\Participantes $participante = null)
    {
        $this->participante = $participante;

        return $this;
    }

    /**
     * Get participante
     *
     * @return \IncGroup\ParticipantesBundle\Entity\Participantes
     */
    public function getParticipante()
    {
        return $this->participante;
    }

    /**
     * Set premio
     *
     * @param string $premio
     *
     * @return Variable
     */
    public function setPremio($premio)
    {
        $this->premio = $premio;

        return $this;
    }

    /**
     * Get premio
     *
     * @return string
     */
    public function getPremio()
    {
        return $this->premio;
    }
}
