prueba_inc_group
================
Generado en la version 2.8 de Symfony.

Para que el proyecto genere las tablas de la base de datos se debe correr en la raiz con el symfony instalado en el servidor el comando:

```
#!symfony

php app/console doctrine:schema:update --force

```

En la parte de ** [Downloads](https://bitbucket.org/mambato/prueba_inc_group/downloads)** se encuentra dos csv (participantes.csv y variables.csv) para probar la aplicación, estos estan separados por ',' algunos programas exportan los csv con ';', tambien esta la estructura en sql de la base de datos.